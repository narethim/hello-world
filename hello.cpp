/**
 * A Test hello.cpp
 *
 * This is a simple example C++ program for demonstration purpose
 */
 
#include <iostream>
 
using namespace std;
 
/**
 * printTest
 */
void printTest(void) {
    cout << "printTest" << endl;

    // Print a number 10 times
    for (int i=0; i<10; i++) {
        printf("Number %d=%d\n", i, i);
    }
    cout  << endl;
}

/**
 * printTestString
 */

void printTestString(void) {
   const char* str[] = { "A string", "B string", "C string" };
    cout << "printTestString" << endl;

    // Print a string 3 times
    for (int i=0; i<3; i++) {
        printf("Number %d=%s\n", i, str[i]);
    }
    cout  << endl;
}


int main(void) {
    cout << "Hello world!" << endl;
    cout  << endl;

    printTest();
    printTestString();

    cout << "end of Hello world!" << endl;

}
